import { IsNotEmpty, IsNumber } from "class-validator";

export class BidDto {
  @IsNumber()
  @IsNotEmpty()
  idAuction:number;
  @IsNumber()
  @IsNotEmpty()
  bidPrice:number;
  @IsNumber()
  @IsNotEmpty()
  idUser:number;
}