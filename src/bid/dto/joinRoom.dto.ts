import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class JoinRoomDto {
  @IsNumber()
  @IsNotEmpty()
  idAuction:number;
  @IsString()
  @IsNotEmpty()
  mess:string;
}
