import { Module } from '@nestjs/common';
import { BidsService } from './service/bid.service';
import { BidGateway } from './bid.gateway';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Bids } from "./entities/bid.entity";
import { User } from "./entities/user.entity";
import { Auctions } from "./entities/auction.entity";
import { Items } from "./entities/item.entity";
import { Transactions } from "./entities/transaction.entity";
import { UserService } from "./service/user.service";
import { TransactionService } from "./service/transaction.service";
import { AuctionService } from "./service/auction.service";



@Module({
  imports: [TypeOrmModule.forFeature([Bids,User,Auctions,Items,Transactions])],
  exports: [TypeOrmModule],
  providers: [BidGateway, BidsService,UserService,TransactionService,AuctionService]
})
export class BidModule {}
