import {
  WebSocketGateway,
  SubscribeMessage,
  WsResponse, WebSocketServer, BaseWsExceptionFilter
} from "@nestjs/websockets";
import { Socket,Server } from 'socket.io';
import { JoinRoomDto } from "./dto/joinRoom.dto";
import { Cron } from "@nestjs/schedule";
import { User } from "./entities/user.entity";
import { UserService } from "./service/user.service";
import { BidDto } from "./dto/bid-dto";
import { Auctions } from "./entities/auction.entity";
import { AuctionService } from "./service/auction.service";
import { TransactionService } from "./service/transaction.service";
import { BidsService } from "./service/bid.service";
import { Bids } from "./entities/bid.entity";
import { Transactions } from "./entities/transaction.entity";
import { UseFilters, UsePipes, ValidationPipe } from "@nestjs/common";
import { AllExceptionsFilter } from "./exception/badrequest.exception";
import { maxDate } from "class-validator";


@WebSocketGateway({cors:true})
export class BidGateway {
  constructor(private readonly userService: UserService,
              private readonly auctionService: AuctionService,
              private readonly transactionService: TransactionService,
              private readonly bidsService: BidsService) {}
  @WebSocketServer() server: Server;

  @UsePipes(new ValidationPipe())
  @UseFilters(new AllExceptionsFilter())
  @SubscribeMessage('joinRoom')
  createRoom(socket: Socket, data: JoinRoomDto): WsResponse<unknown> {
    socket.join(data.idAuction.toString());
    socket.to(data.idAuction.toString()).emit('joinRoomReceive', {data: data.mess+' joined a room'});
    return { event: 'joinRoomReceive', data:"u joined a room" };
  }
  @UsePipes(new ValidationPipe())
  @UseFilters(new AllExceptionsFilter())
  @SubscribeMessage('bid')
  async bid(socket: Socket, bidDto: BidDto) {
    const user :User = await this.userService.findOne(bidDto.idUser);
    const auction:Auctions = await this.auctionService.findOne(bidDto.idAuction);
    let bidEntity :Bids = new Bids();
    bidEntity.user=user;
    bidEntity.auctions=auction;
    bidEntity.bid_price= bidDto.bidPrice;
    const bid :Bids=await this.bidsService.save(bidEntity);
    this.auctionService.updateCurrentPrice(bidDto.idAuction,bidDto.bidPrice);
    this.server.to(bidDto.idAuction.toString()).emit('bidReceive', { mess:user.username + " bid " + bidDto.bidPrice,currentPrice:bidDto.bidPrice });
  }
  @Cron('* * * * * *')
  async handleCron() {
    const now:number = Date.now();
    const auctionArr :Auctions[] = await this.auctionService.findAll();
    for (const auction of auctionArr) {
        const endDate :Date = auction.timeEnd;
        const startDate:Date = auction.timeStart;
        if(startDate.getTime()<now && auction.status!=1){
          this.auctionService.updateStatus(auction.ID,1);
        }
        if(endDate.getTime()<now){
          this.auctionService.updateStatus(auction.ID,-1);
          const maxBids :Bids = await this.bidsService.findmaxBidinAuction(auction.ID);
          if(JSON.stringify(maxBids) !== '{}'){
            let transactionEntity :Transactions = new Transactions();
            transactionEntity.auction = auction;
            transactionEntity.user = maxBids.user;
            transactionEntity.paid = maxBids.bid_price;
            const transaction :Transactions = await this.transactionService.save(transactionEntity);
            const user :User = await this.userService.updateBalance(maxBids.user.ID, maxBids.bid_price);
            const winnerauction: Auctions = await this.auctionService.updateWinner(auction.ID, user);
            this.server.emit("checkEndedReceive", { mess:"Auction Ended and "+user.username+" is Winner",maxBidPrice :maxBids.bid_price });
          }
        }
      }
    }
}
