
import { Catch, ArgumentsHost } from '@nestjs/common';
import { BaseWsExceptionFilter, WebSocketGateway, WebSocketServer } from "@nestjs/websockets";
import { Server, Socket } from "socket.io";

@Catch()
@WebSocketGateway({cors:true})
export class AllExceptionsFilter extends BaseWsExceptionFilter {
  @WebSocketServer() server: Server;
  catch(exception: unknown, host: ArgumentsHost) {
    console.log(host.getArgByIndex(0));
    const socket:Socket =host.getArgs()[0];
    socket.emit("exception",exception["response"].message)
  }
}

