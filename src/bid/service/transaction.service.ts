import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm';
import { Transactions } from "../entities/transaction.entity";

@Injectable()
export class TransactionService {
  constructor(
    @InjectRepository(Transactions)
    private transactionsRepository: Repository<Transactions>,
  ) {}
  async save(transactions:Transactions) :Promise<Transactions> {
    return await this.transactionsRepository.save(transactions);
  }
}