import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm'
import { Repository, SelectQueryBuilder } from "typeorm";
import { Bids } from "../entities/bid.entity";

@Injectable()
export class BidsService {
  constructor(
    @InjectRepository(Bids)
    private bidsRepository: Repository<Bids>,
  ) {}
  async findmaxBidinAuction(id_auction :number): Promise<Bids>{
    const listBid : Bids[] =await this.bidsRepository.createQueryBuilder("bids")
      .where("bids.id_auction = :id_auction",{id_auction})
      .innerJoinAndSelect("bids.user", "user")
      .innerJoinAndSelect("bids.auctions", "auctions")
      .getMany();
    let maxPrice =0;
    let maxBid :Bids = new Bids();
    listBid.forEach(bid =>{
      if(bid.bid_price>maxPrice){
        maxPrice=bid.bid_price;
        maxBid =bid;
      }
    })
    return maxBid;
  }

  async save(bid:Bids) :Promise<Bids> {
    return await this.bidsRepository.save(bid);
  }
  async remove(id: string): Promise<void> {
    await this.bidsRepository.delete(id);
  }
}