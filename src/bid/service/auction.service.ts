import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm';
import { Auctions } from "../entities/auction.entity";
import { User } from "../entities/user.entity";
import { Bids } from "../entities/bid.entity";

@Injectable()
export class AuctionService {
  constructor(
    @InjectRepository(Auctions)
    private auctionsRepository: Repository<Auctions>,
  ) {}
   async findAll(): Promise<Auctions[]> {
    const listBid : Auctions[] =await this.auctionsRepository.createQueryBuilder("bids")
      .where("bids.is_delete <> 1 and bids.status <> -1")
      .getMany();
    return listBid;
  }
  findOne(id: number): Promise<Auctions> {
    return this.auctionsRepository.findOneById(id);
  }
  async updateWinner(id: number, winner: User): Promise<Auctions> {
    const auctions = await this.auctionsRepository.findOneById(id);
    if (!auctions) {
      throw new NotFoundException('Auctions is not found');
    }
    auctions.winner=winner;
    await auctions.save();
    return auctions;
  }
  async updateStatus(id: number, status: number): Promise<Auctions> {
    const auctions = await this.auctionsRepository.findOneById(id);
    if (!auctions) {
      throw new NotFoundException('Auctions is not found');
    }
    auctions.status=status;
    await auctions.save();
    return auctions;
  }
  async updateCurrentPrice(id: number, bidPrice: number): Promise<Auctions> {
    const auctions = await this.auctionsRepository.findOneById(id);
    if (!auctions) {
      throw new NotFoundException('Auctions is not found');
    }
    auctions.currentPrice=bidPrice;
    await auctions.save();
    return auctions;
  }
}