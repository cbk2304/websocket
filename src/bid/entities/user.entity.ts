import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import { timestamp } from "rxjs";
import { Bids } from "./bid.entity";
import { Transactions } from "./transaction.entity";
import { Auctions } from "./auction.entity";

@Entity({ name: 'users' })
export class User extends BaseEntity{
  constructor() {
    super();
  }
  @PrimaryGeneratedColumn()
  ID: number;
  @Column({name:"username"})
  username:string;
  @Column({name:"password"})
  password:string;
  @Column({name:"address"})
  address:string;
  @Column({name:"name"})
  name:string;
  @Column({name:"email"})
  email:string;
  @Column({name:"dob"})
  dob:string;
  @Column({name:"name_avatar"})
  nameAvatar:string;
  @Column({name:"role"})
  role:string;
  @Column({name:"balance"})
  balance:number;
  @CreateDateColumn({name:"creat_at",type:'timestamp',nullable:false})
  creaAt: Date;
  @UpdateDateColumn({name:"modify_at",type:'timestamp',nullable:false})
  modifiAt:Date;
  @OneToMany(() => Bids, (bids) => bids.user)
  bid?: Bids[];
  @OneToMany(() => Transactions, (transactions) => transactions.user)
  transactions?: Transactions[];
  @OneToMany(() => Auctions, (auctions) => auctions.userCreate)
  auctionCreated?: Auctions[];
  @OneToMany(() => Auctions, (auctions) => auctions.winner)
  auction?: Auctions[];


}
