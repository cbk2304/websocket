import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn, BaseEntity
} from "typeorm";
import { User } from "./user.entity";
import { Auctions } from "./auction.entity";

@Entity({ name: 'bids' })
export class Bids extends BaseEntity{
  constructor() {
    super();
  }
  @PrimaryGeneratedColumn()
  ID: number;
  @Column({name:"bid_price"})
  bid_price :number;
  @CreateDateColumn({name:"creat_at",type:'timestamp'})
  creatAt: Date;
  @UpdateDateColumn({name:"modify_at",type:'timestamp'})
  modifyAt:Date;
  @ManyToOne(() => User, (user) => user.bid, { nullable: false })
  @JoinColumn({name: 'id_user' })
  user?: User;
  @ManyToOne(() => Auctions, (auctions) => auctions.bids, { nullable: false })
  @JoinColumn({name: 'id_auction' })
  auctions?: Auctions;


}
