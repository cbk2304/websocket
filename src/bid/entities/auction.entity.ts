import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne, OneToMany, OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import { User } from "./user.entity";
import { Transactions } from "./transaction.entity";
import { Bids } from "./bid.entity";
import { Items } from "./item.entity";

@Entity({ name: 'auctions' })
export class Auctions extends BaseEntity{
  constructor() {
    super();
  }
  @PrimaryGeneratedColumn()
  ID: number;
  @Column({name:"init_price"})
  initPrice :number;
  @Column({name:"current_price"})
  currentPrice :number;
  @Column({name:"time_start",type:'timestamp'})
  timeStart :Date;
  @Column({name:"time_end",type:'timestamp'})
  timeEnd :Date;
  @Column({ name : "is_delete"})
  isDelete:number;
  @CreateDateColumn({name:"creat_at",type:'timestamp'})
  creatAt: Date;
  @UpdateDateColumn({name:"modify_at",type:'timestamp'})
  modifyAt:Date;
  @Column({name:"status"})
  status :number;
  @OneToMany(() => Transactions, (transactions) => transactions.auction)
  transactions?: Transactions[];
  @OneToMany(() => Bids, (bids) => bids)
  bids?: Bids[];
  @OneToOne(() => Items,{ nullable: false })
  @JoinColumn({ name: "id_item" })
  item: Items
  @ManyToOne(() => User, (userCreate) => userCreate.auctionCreated, { nullable: false })
  @JoinColumn({name: 'id_user_create' })
  userCreate?: User;
  @ManyToOne(() => User, (winner) => winner.auction, { nullable: true })
  @JoinColumn({name: 'id_winner' })
  winner?: User;
}