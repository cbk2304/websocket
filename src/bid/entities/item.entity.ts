import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
@Entity({ name: 'items' })
export class Items extends BaseEntity{
  constructor() {
    super();
  }
  @PrimaryGeneratedColumn()
  ID: number;
  @Column({ name : "name"})
  name:string;
  @Column({ name: "description" })
  description:string;
  @Column({ name : "name_image" })
  nameImage:string;
  @Column({ name : "is_delete"})
  isDelete:number;
  @Column({ name : "creat_at" })
  @CreateDateColumn({name:"creat_at",type:'timestamp'})
  creatAt: Date;
  @UpdateDateColumn({name:"modify_at",type:'timestamp'})
  modifyAt:Date;

}