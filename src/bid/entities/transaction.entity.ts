import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import { User } from "./user.entity";
import { Auctions } from "./auction.entity";

@Entity({ name: 'transactions' })
export class Transactions extends BaseEntity{
  constructor() {
    super();
  }
  @PrimaryGeneratedColumn()
  ID: number;
  @Column({ name: "paid"})
  paid:number;
  @CreateDateColumn({name:"creat_at",type:'timestamp'})
  creatAt: Date;
  @UpdateDateColumn({name:"modify_at",type:'timestamp'})
  modifyAt:Date;
  @ManyToOne(() => User, (user) => user.transactions, { nullable: false })
  @JoinColumn({name: 'id_user' })
  user?: User;
  @ManyToOne(() => Auctions, (auction) => auction.transactions, { nullable: false })
  @JoinColumn({name: 'id_auction' })
  auction?: Auctions;

}