import { Module } from '@nestjs/common';
import { BidModule } from './bid/bid.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { Bids } from "./bid/entities/bid.entity";
import { User } from "./bid/entities/user.entity";
import { Items } from "./bid/entities/item.entity";
import { Transactions } from "./bid/entities/transaction.entity";
import { Auctions } from "./bid/entities/auction.entity";
import { ScheduleModule } from "@nestjs/schedule";

@Module({
  imports: [
    ScheduleModule.forRoot(),
    BidModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '23042000',
      database: 'test',
      entities: [
        Bids,User,Items,Transactions,Auctions
      ],
      synchronize: true,
      autoLoadEntities:true,
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
